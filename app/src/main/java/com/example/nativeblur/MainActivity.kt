package com.example.nativeblur

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.nativeblur.databinding.ActivityMainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var radius = 1
    private var xPosition = 0
    private var yPosition = 0

    private lateinit var blurredJob: Job

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.sampleImage.setOnTouchListener { v, event ->
            xPosition = event.x.toInt()
            yPosition = event.y.toInt()
            return@setOnTouchListener true
        }

        binding.seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {

            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                // you can probably leave this empty
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                radius = seekBar.progress
            }
        })


        binding.blurBtn.setOnClickListener {
            binding.apply {
                sampleImage.visibility = View.GONE
                loading.visibility = View.VISIBLE
                blurBtn.isEnabled = false
                seekbarLayout.visibility = View.GONE
                blurOutOfCircleBtn.visibility = View.GONE
            }

            executeNativeBlur()
        }

        binding.blurOutOfCircleBtn.setOnClickListener {
            // ensure bitmap is mutable
            val originalBitmap = BitmapFactory.decodeResource(resources, R.drawable.img)
            val mutableBitmap = originalBitmap.copy(Bitmap.Config.ARGB_8888, true)
            if (::blurredJob.isInitialized && blurredJob.isActive)  blurredJob.cancel()
            blurredJob = CoroutineScope(Dispatchers.Main).launch {
                applyNativeBlur(mutableBitmap)
            }
        }

    }

    private suspend fun applyNativeBlur(bitmap: Bitmap) {
        Log.i(TAG, "applyNativeBlur: called")
        val resultBitmap = withContext(Dispatchers.Default) {
            getCoordinates(bitmap, xPosition, yPosition, radius)
        }
        binding.sampleImage.setImageBitmap(resultBitmap)
    }

    private fun executeNativeBlur() {
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.img)

        lifecycleScope.launch {
            withContext(Dispatchers.Default) {
                applyGaussianBlur(bitmap)
            }?.let { blurredBitmap ->
                binding.apply {
                    loading.visibility = View.GONE
                    sampleImage.visibility = View.VISIBLE
                    sampleImage.setImageBitmap(blurredBitmap)
                    blurBtn.isEnabled = true
                    blurOutOfCircleBtn.visibility = View.VISIBLE
                    seekbarLayout.visibility = View.VISIBLE
                }
            }
        }
    }

    /**
     * A native method that is implemented by the 'nativeblur' native library,
     * which is packaged with this application.
     */
//    external fun stringFromJNI(): String
    private external fun applyGaussianBlur(bitmap: Bitmap?): Bitmap?
    private external fun getCoordinates(bitmap: Bitmap?,x: Int, y: Int, r: Int): Bitmap

    companion object {
        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("NativeBlur")
        }

        private const val TAG = "MainActivity"
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::blurredJob.isInitialized)
            blurredJob.cancel()
    }

}