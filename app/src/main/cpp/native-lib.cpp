#include <jni.h>
#include <string>
#include <android/log.h>
#include <cmath>
#include <android/bitmap.h>
#include <cstdint>
#include <iostream>
#include <stdint.h>
#include <vector>


#define TAG "NATIVE_LIB"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG,    TAG, __VA_ARGS__)

using namespace std;

bool isInCircle(int centerX, int centerY, int radius, int x, int y)
{
    int res = ((x - centerX) * (x - centerX)) + ((y - centerY) * (y - centerY));
    return res <= (radius * radius);
}

void applyGaussianBlur(uint32_t* pixels, int width, int height, int radius) {
//    LOG_TAG("", "", "");
    int kernelSize = 2 * radius + 1;
    vector<float> kernel(kernelSize);
    float sigma = radius / 2.0;
    float sum = 0.0;

    for (int i = 0; i < kernelSize; ++i)
    {
        float x = i - radius;
        kernel[i] = exp(-0.05 * (x * x) / (sigma * sigma));
        sum += kernel[i];
    }

    for (float& value : kernel)
        value /= sum;

    vector<uint32_t> tempPixels(width * height);

    // Horizontal pass
    for (int y = 0; y < height; ++y)
    {
        for (int x = 0; x < width; ++x)
        {
            float r = 0, g = 0, b = 0;
            for (int k = -radius; k <= radius; ++k)
            {
                int px = min(max(x + k, 0), width - 1);
                uint32_t pixel = pixels[y * width + px];
                float weight = kernel[k + radius];
                r += weight * ((pixel >> 16) & 0xFF);
                g += weight * ((pixel >> 8) & 0xFF);
                b += weight * (pixel & 0xFF);
            }
            uint32_t newPixel = (0xFF << 24) | ((int)r << 16) | ((int)g << 8) | (int)b;
            tempPixels[y * width + x] = newPixel;
        }
    }

    // Vertical pass
    for (int y = 0; y < height; ++y)
    {
        for (int x = 0; x < width; ++x)
        {
            float r = 0, g = 0, b = 0;

            for (int k = -radius; k <= radius; ++k)
            {
                int py = min(max(y + k, 0), height - 1);
                uint32_t pixel = tempPixels[py * width + x];
                float weight = kernel[k + radius];
                r += weight * ((pixel >> 16) & 0xFF);
                g += weight * ((pixel >> 8) & 0xFF);
                b += weight * (pixel & 0xFF);
            }
            uint32_t newPixel = (0xFF << 24) | ((int)r << 16) | ((int)g << 8) | (int)b;
            pixels[y * width + x] = newPixel;
        }
    }
}

class Circle
{
    int x; int y; int r;
    public:int getR() { return r;}
    public:int getX() {return x;}
    public:int getY() {return y;}
    Circle(int x, int y,int r)
    {
        this->x = x;
        this->y = y;
        this->r = r;
    }
};

void applyGaussianBlurUsingCircle(uint32_t* pixels, int width, int height, Circle &circle)
{
    __android_log_print(ANDROID_LOG_VERBOSE, TAG, "applyGaussianBlurUsingCircle called.");
    int radius = 8;
    int kernelSize = 2 * radius + 1;
    vector<float> kernel(kernelSize);
    float sigma = radius / 2.0;
    float sum = 0.0;

    float circleCenterX, circleCenterY;
    float circleRadius = circle.getR();

    for (int i = 0; i < kernelSize; ++i)
    {
        float x = i - radius;
        kernel[i] = exp(-0.005 * (x * x) / (sigma * sigma));
        sum += kernel[i];
    }

    for (float& value : kernel)
        value /= sum;

    vector<uint32_t> tempPixels(width * height);
    circleCenterX = circle.getX();
    circleCenterY = circle.getY();

    // Horizontal pass
    for (int y = 0; y < height; ++y)
    {
        for (int x = 0; x < width; ++x)
        {
            float r = 0, g = 0, b = 0;
            bool flag = false;
            for (int k = -radius; k <= radius; ++k)
            {
                if (isInCircle(circleCenterX, circleCenterY, circleRadius, x, y))
                {
                    flag = true;
                    continue;
                }
                int px = min(max(x + k, 0), width - 1);
                uint32_t pixel = pixels[y * width + px];
                float weight = kernel[k + radius];
                r += weight * ((pixel >> 16) & 0xFF);
                g += weight * ((pixel >> 8) & 0xFF);
                b += weight * (pixel & 0xFF);
            }
            if (!flag)
            {
                uint32_t newPixel = (0xFF << 24) | ((int)r << 16) | ((int)g << 8) | (int)b;
                tempPixels[y * width + x] = newPixel;
            }
        }
    }

    // Vertical pass
    for (int y = 0; y < height; ++y)
    {
        for (int x = 0; x < width; ++x)
        {
            float r = 0, g = 0, b = 0;
            bool flag = false;
            for (int k = -radius; k <= radius; ++k)
            {
                if (isInCircle(circleCenterX, circleCenterY, circleRadius, x, y))
                {
                    flag = true;
                    continue;
                }
                int py = min(max(y + k, 0), height - 1);
                uint32_t pixel = tempPixels[py * width + x];
                float weight = kernel[k + radius];
                r += weight * ((pixel >> 16) & 0xFF);
                g += weight * ((pixel >> 8) & 0xFF);
                b += weight * (pixel & 0xFF);
            }
            if (!flag)
            {
                uint32_t newPixel = (0xFF << 24) | ((int)r << 16) | ((int)g << 8) | (int)b;
                pixels[y * width + x] = newPixel;
            }
        }
    }
}


extern "C"

JNIEXPORT jobject JNICALL
Java_com_example_nativeblur_MainActivity_applyGaussianBlur(JNIEnv* env, jobject obj, jobject bitmap)
{
    AndroidBitmapInfo info;
    void* pixels;

    if (AndroidBitmap_getInfo(env, bitmap, &info) != ANDROID_BITMAP_RESULT_SUCCESS)
    {
        return nullptr;
    }

    if (info.format != ANDROID_BITMAP_FORMAT_RGBA_8888)
    {
        return nullptr;
    }

    if (AndroidBitmap_lockPixels(env, bitmap, &pixels) != ANDROID_BITMAP_RESULT_SUCCESS)
    {
        return nullptr;
    }

    applyGaussianBlur((uint32_t*)pixels, info.width, info.height, 8);
    AndroidBitmap_unlockPixels(env, bitmap);


    return bitmap;
}

extern "C"
JNIEXPORT jobject
Java_com_example_nativeblur_MainActivity_getCoordinates(JNIEnv* env,jobject obj, jobject bitmap, jint x, jint y, jint radius)
{
    AndroidBitmapInfo info;
    void* pixels;

    if (AndroidBitmap_getInfo(env, bitmap, &info) != ANDROID_BITMAP_RESULT_SUCCESS)
    {
        return nullptr;
    }

    if (info.format != ANDROID_BITMAP_FORMAT_RGBA_8888)
    {
        return nullptr;
    }

    if (AndroidBitmap_lockPixels(env, bitmap, &pixels) != ANDROID_BITMAP_RESULT_SUCCESS)
    {
        return nullptr;
    }

    Circle circle(x, y, radius);
    __android_log_print(ANDROID_LOG_VERBOSE, TAG, "radius: %d", radius);
    __android_log_print(ANDROID_LOG_VERBOSE, TAG, "radius^2: %d", (radius * radius));
    applyGaussianBlurUsingCircle((uint32_t*)pixels, info.width, info.height, circle);
    AndroidBitmap_unlockPixels(env, bitmap);

    return bitmap;
}


extern "C"
JNIEXPORT jstring JNICALL

Java_com_example_nativeblur_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject *obj
) {
    string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
